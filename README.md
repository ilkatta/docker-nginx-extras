# Dockerfile Nginx Extras - last nginx mainline release with extra modules.

## Integrated modules:

- file-aio
- google_perftools_module
- http_addition_module
- http_dav_module
- http_degradation_module
- http_flv_module
- http_geoip_module
- http_gunzip_module
- http_gzip_static_module
- http_image_filter_module
- http_mp4_module
- http_perl_module
- http_random_index_module
- http_realip_module
- http_secure_link_module
- http_secure_link_module
- http_spdy_module
- http_ssl_module
- http_stub_status_module
- http_sub_module
- http_xslt_module
- ipv6
- mail
- mail_ssl_module
- pcre-jit
- nginscript

## Third party

- array var
- auth pam
- cache purge
- coolkit
- dav ext
- drizzle
- echo
- encrypted session
- fancy index
- form input
- http auth request
- http headers more
- http substitutions filter
- iconv
- iconv
- lua
- lua resty mysql ( html { lua_package_path '/usr/src/lua-resty-mysql/lib/?.lua;;'; [...] } )
- memc
- modsecurity
- pagespeed
- postgres
- set misc
- upload-progress
- upstream fair
